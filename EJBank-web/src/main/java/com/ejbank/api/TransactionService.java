package com.ejbank.api;


import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ejbank.payload.input.TransactionRequest;
import com.ejbank.payload.input.TransactionSummary;
import com.ejbank.payload.input.TransactionValidate;
import com.ejbank.payload.output.StatusSummaryTransaction;
import com.ejbank.payload.output.TransactionList;
import com.ejbank.payload.output.TransactionResult;
import com.ejbank.payload.output.TransactionValidationResult;
import com.ejbank.session.TransactionSession;

@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class TransactionService{
	@EJB
	private TransactionSession transaction;
	
	
	
	@GET
	@Path("/validation/notification/{user_id}")
	public Long numberOfTransaction(@PathParam("user_id") Integer user_id){
		return transaction.checkBeforeGetNumberOfTransactionToValidate(user_id);
	}

	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/preview")
	public StatusSummaryTransaction createCustomer(TransactionSummary summary) {
		return transaction.checkSummary(summary);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public TransactionResult result(TransactionRequest request){
		return transaction.checkBeforeExecuteOp(request);
	}
	
	@GET
	@Path("/list/{account_id}/{offset}/{user_id}")
	public TransactionList getListTransaction(@PathParam("account_id") Integer account_id,@PathParam("offset") Integer offset,@PathParam("user_id") Integer user_id) {
		return transaction.checkBeforeGetListTransaction(account_id, offset, user_id);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/validation")
	public TransactionValidationResult validateAdvisorTransaction(TransactionValidate request) {
		return transaction.checkBeforeValidateTransaction(request);
	}
	
}
