package com.ejbank.api;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ejbank.payload.output.AccountDetail;
import com.ejbank.session.AccountSession;

@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class AccountDetailService {
	@EJB
	private AccountSession account;
	
	@GET
	@Path("/{account_id}/{user_id}")
	public AccountDetail account_detail(@PathParam("account_id") Integer account_id,@PathParam("user_id") Integer user_id){
		return account.getDetailAccount(account_id,user_id);
	}
	
}
