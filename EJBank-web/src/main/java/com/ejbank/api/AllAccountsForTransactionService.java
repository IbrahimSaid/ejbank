package com.ejbank.api;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ejbank.payload.output.AccountList;
import com.ejbank.payload.output.AccountTransaction;
import com.ejbank.session.TransactionSession;

@Path("/accounts/all")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class AllAccountsForTransactionService {
	@EJB
	private TransactionSession transaction;

	@GET
	@Path("/{user_id}")
	public AccountList<AccountTransaction> allAccountsForTransaction(@PathParam("user_id") Integer user_id){
		return transaction.checkBeforeGetAllAccounts(user_id);
	}
}
