package com.ejbank.api;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.PathParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ejbank.payload.output.UserName;
import com.ejbank.session.UserSession;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class UserService {
	@EJB
	private UserSession user;

	@GET
	@Path("/{user_id}")
	public UserName testUser(@PathParam("user_id") Integer id){
		return user.findUserById(id);
	}
}