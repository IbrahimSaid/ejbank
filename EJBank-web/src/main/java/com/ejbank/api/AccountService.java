package com.ejbank.api;



import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ejbank.payload.output.AccountAttached;
import com.ejbank.payload.output.AccountList;
import com.ejbank.payload.output.AccountUser;
import com.ejbank.session.AccountSession;

@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class AccountService {
	@EJB
	private AccountSession account;
	
	@GET
	@Path("/{user_id}")
	public AccountList<AccountUser> accounts(@PathParam("user_id") Integer id){
		return account.getAccountsFromUser(id);
	}
	
	@GET
	@Path("/attached/{user_id}")
	public AccountList<AccountAttached> accountsToAdvisor(@PathParam("user_id") Integer id){
		return account.getAccountsToAdvisor(id);
	}
}
