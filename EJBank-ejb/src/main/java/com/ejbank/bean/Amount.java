package com.ejbank.bean;

public class Amount {
	private Double amount;

	public Amount (Double amount) {
		this.amount=amount;
	}
	
	public void add(Double amount) {
		this.amount+=amount;
	}
	
	public void sub(Double amount) {
		this.amount-=amount;
	}
	public Double getAmount() {
		return amount;
	}

}