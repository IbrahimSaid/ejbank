package com.ejbank.bean;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.ejbank.entity.User;
import com.ejbank.payload.output.UserName;
import com.ejbank.session.UserSession;

@Stateless
@LocalBean
public class UserBean implements UserSession {
	@PersistenceContext(unitName="EJBankPU")
	private EntityManager em;
	
	@Override
	public UserName findUserById(Integer id) {
		User tmp=em.find(User.class, id);
		return new UserName(tmp.getLastname(), tmp.getFirstname());
	}

}