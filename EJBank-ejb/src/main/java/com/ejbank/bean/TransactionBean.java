package com.ejbank.bean;

import java.sql.Date;
import java.time.LocalDate;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.UserTransaction;

import com.ejbank.bean.State;
import com.ejbank.entity.Account;
import com.ejbank.entity.Advisor;
import com.ejbank.entity.Customer;
import com.ejbank.entity.Transaction;
import com.ejbank.entity.User;
import com.ejbank.payload.input.TransactionRequest;
import com.ejbank.payload.input.TransactionSummary;
import com.ejbank.payload.input.TransactionValidate;
import com.ejbank.payload.output.AccountList;
import com.ejbank.payload.output.AccountTransaction;
import com.ejbank.payload.output.StatusSummaryTransaction;
import com.ejbank.payload.output.TransactionList;
import com.ejbank.payload.output.TransactionPayload;
import com.ejbank.payload.output.TransactionResult;
import com.ejbank.payload.output.TransactionValidationResult;
import com.ejbank.session.TransactionSession;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class TransactionBean implements TransactionSession{
	private final Long MAX_VIREMENT=(long) 1000;
	private final Integer LIMIT_TRANSACTION=2;
	
	@PersistenceContext(unitName="EJBankPU")
	private EntityManager em;
	
	@Resource
    private UserTransaction tx;
	
	/**
	 * @param user_id : Advisor ID
	 * Verify if Advisor attached to ID param
	 * 
	 * */
	
	@Override
	public Long checkBeforeGetNumberOfTransactionToValidate(Integer user_id) {
		Advisor advisor=em.find(Advisor.class, user_id);
		if(advisor==null) {
			return (long) 0;
		}
		return getNumberOfTransactionToValidate(user_id);
	}
	
	/***
	 * Return number of transaction to validate for an Advisor
	 * @param user_id : Advisor ID
	 */
	private Long getNumberOfTransactionToValidate(Integer user_id) {
		TypedQuery<Long> query=em.createNamedQuery(Advisor.findTotalTransactionInProgress,Long.class);
		query.setParameter("advisorId", user_id);
		return query.getSingleResult();
	}

	/**
	 * @param user_id : User ID
	 * Verify if there is User attached to ID param.
	 * 
	 * */
	@Override
	public AccountList<AccountTransaction> checkBeforeGetAllAccounts(Integer user_id) {
		User user=findUser(user_id);
		AccountList<AccountTransaction> result=new AccountList<>();
		if(user==null) {
			result.setError("User not found");
			return result;
		}
		return allAccounts();
	}
	
	/**
	 * Return all accounts
	 * */
	private AccountList<AccountTransaction> allAccounts(){
		AccountList<AccountTransaction> result=new AccountList<>();
		TypedQuery<Account> query=em.createNamedQuery(Account.findAllAccounts,Account.class);
		for(Account a:query.getResultList()) {
			result.add(new AccountTransaction(a.getId(), a.getCustomer().getFirstname()+a.getCustomer().getLastname(), a.getAccount_type().getName(), a.getBalance()));
		}
		return result;
	}
	
	/**
	 * @param Input payload Transactionsummary
	 * return a status of current transaction
	 * */
	
	@Override
	public StatusSummaryTransaction checkSummary(TransactionSummary summary) {
		Account src=findAccount(summary.getSource());
		Account dest=findAccount(summary.getDestination());
		User author=findUser(summary.getAuthor());
		
		if(src==null) {
			return new StatusSummaryTransaction("Account source : "+summary.getSource()+ " does not exist");
		}
		if(dest==null) {
			return new StatusSummaryTransaction("Account destination : "+summary.getDestination()+ " does not exist");
		}
		if(author==null) {
			return new StatusSummaryTransaction("Author : "+summary.getAuthor()+ " does not exist");
		}
		
		if(src.getCustomer().getId()!=author.getId() && src.getCustomer().getAdvisor().getId()!=author.getId()) {
			return new StatusSummaryTransaction("Author : "+author.getId()+ " does not have access to source account");
		}
		if(src.isOverdraft(summary.getAmount())) {
			return new StatusSummaryTransaction(false, src.getBalance()-summary.getAmount(), dest.getBalance()+summary.getAmount(), "Découvert dépassé");
		}
		return new StatusSummaryTransaction(true, src.getBalance()-summary.getAmount(), dest.getBalance()+summary.getAmount(), "Virement possible");
	}
	
	
	/**
	 * @param Input payload TransactionRequest
	 * Verify src-account, dest-account, author of current transaction
	 * */
	
	
	@Override
	public TransactionResult checkBeforeExecuteOp(TransactionRequest transaction)   {
		Account src=findAccount(transaction.getSource());
		Account dest=findAccount(transaction.getDestination());
		User author=findUser(transaction.getAuthor());
		
		if(src==null) {
			return new TransactionResult(false,"Account source : "+transaction.getSource()+ " does not exist");
		}
		if(dest==null) {
			return new TransactionResult(false,"Account destination : "+transaction.getDestination()+ " does not exist");
		}
		if(author==null) {
			return new TransactionResult(false,"Author : "+transaction.getAuthor()+ " does not exist");
		}
		
		if(src.getCustomer().getId()!=author.getId() && src.getCustomer().getAdvisor().getId()!=author.getId()) {
			return new TransactionResult(false,"Author : "+author.getId()+ " does not have access to source account");
		}
		
		if(src.isOverdraft(transaction.getAmount())) {
			return new TransactionResult(false, "compte à découvert");
		}
		return  executeOp(transaction, author, src, dest);
	}
	
	
	/**
	 * @param payload TransactionRequest, authorn,src-account,dest-account
	 * execute current transaction if possible
	 * 
	 * */
	private TransactionResult executeOp(TransactionRequest transaction,User author,Account src,Account dest) {
		if(isCustomer(author) && isOverMax(transaction.getAmount())) {
			try {
				tx.begin();
				Transaction virement=new Transaction(em.getReference(Account.class, src.getId()), dest.getId(), author.getId(),transaction.getAmount(), transaction.getComment(),0, Date.valueOf(LocalDate.now()));
				em.persist(virement);
				em.flush();
				tx.commit();
				return new TransactionResult(true, "virement supérieur à 1000 en attente de validation");
			} catch (Exception e) {
				try {
					tx.rollback();
				} catch(Exception e1) {}
				return new TransactionResult(false, "Internal error");
			}
		}
		try {
			tx.begin();
			Transaction virement=new Transaction(em.getReference(Account.class, src.getId()), dest.getId(), author.getId(),transaction.getAmount(), transaction.getComment(),1, Date.valueOf(LocalDate.now()));
			em.persist(virement);
			dest.credit(transaction.getAmount());
			src.debit(transaction.getAmount());
			em.merge(src);
			em.merge(dest);
			em.flush();
			tx.commit();
			return new TransactionResult(true, "virement effectué");
		} catch (Exception e) {
			try {
				tx.rollback();
			} catch(Exception e1) {}
			return new TransactionResult(false, "Internal error");
		}
	}
	
	
	/**
	 * @param account_id,offset,userid
	 * Verify entities associated to params
	 * */

	@Override
	public TransactionList checkBeforeGetListTransaction(Integer account_id, Integer offset, Integer user_id) {
		TransactionList ts=new TransactionList();
		User user=findUser(user_id);
		if(user==null) {
			ts.setError("User is not recognized in the system");
			return ts;
		}
		Account account=findAccount(account_id);
		if(account==null) {
			ts.setError("Account number is not recognized");
			return ts;
		}
		
		if(account.getCustomer().getId()!=user.getId() && account.getCustomer().getAdvisor().getId()!=user.getId()) {
			ts.setError("Error : User does not have acces to this account");
			return ts;
		}
		return getListTransaction(user, account, offset);
	}
	
	
	
	/**
	 * 
	 * Return list of transaction
	 * */
	private TransactionList getListTransaction(User user,Account account,Integer offset) {
		TransactionList ts=new TransactionList();
		TypedQuery<Long> totalTrans=em.createNamedQuery(Transaction.findTolalTransactionAccount,Long.class);
		totalTrans.setParameter("accountId", account.getId());
		ts.setTotal(totalTrans.getSingleResult());
		
		TypedQuery<Transaction> trans=em.createNamedQuery(Transaction.findUserTransactionList,Transaction.class);
		trans.setParameter("accountId", account.getId());
		trans.setMaxResults(LIMIT_TRANSACTION);
		trans.setFirstResult(offset);
		for(Transaction t : trans.getResultList()) {
			Account src=t.getCompte();
			Account dest=findAccount(t.getAccount_id_to());
			User author=findUser(t.getAuthor());
			TransactionPayload payload=new TransactionPayload(t.getId(), t.getDate(), src.getAccount_type().getName(),
					dest.getAccount_type().getName(), t.getAmount(), author.getFirstname()+""+author.getLastname(), t.getComment());
			if(t.getApplied()==1) {
				payload.setState(State.APPLYED.toString());
			}
			else  {
				if(isCustomer(user)) {
					payload.setState(State.WAITING_APPROVE.toString());
				}else {
					payload.setState(State.TO_APPROVE.toString());
				}
			}
			
			if(src.getCustomer().getId()==user.getId() || src.getCustomer().getAdvisor().getId()==user.getId()) {
				payload.setDestination_user(dest.getCustomer().getFirstname()+" "+dest.getCustomer().getLastname());
			}
			else {
				payload.setSource_user(src.getCustomer().getFirstname()+" "+src.getCustomer().getLastname());
			}
			ts.add(payload);
		}
		return ts;
	}
	
	
	
	/**
	 * @param input payload TransactionValidate
	 * Verify user and transaction entities associated to the payload 
	 * */
	@Override
	public TransactionValidationResult checkBeforeValidateTransaction(TransactionValidate request) {
		TransactionValidationResult result=new TransactionValidationResult();
		User user=findUser(request.getAuthor());
		if(user==null) {
			result.setResult(false);
			result.setError("User not recognized");
			return result;
		}
		Transaction transaction=em.find(Transaction.class, request.getTransaction());
		if(transaction==null) {
			result.setResult(false);
			result.setError("Transaction not found");
			return result;
		}
		if(transaction.getCompte().getCustomer().getAdvisor().getId()!=user.getId()) {
			result.setResult(false);
			result.setError("Operation unautorized for this user");
			return result;
		}
		return validateTransaction(request,transaction);
	}
	
	
	/**
	 * @param Input payload TransactionValidate and 
	 * Validate or cancel the transction passed in param
	 * */
	private TransactionValidationResult validateTransaction(TransactionValidate request,Transaction transaction) {
		TransactionValidationResult result=new TransactionValidationResult();
		if(!request.isApprove()) {
			try {
				tx.begin();
				if(!em.contains(transaction)) {
					transaction=em.merge(transaction);
				}
				em.remove(transaction);
				result.setResult(true);
				result.setMessage("Transaction successfully canceled");
				em.flush();
				tx.commit();
				return result;
			}catch (Exception e) {
				try {
					tx.rollback();
				} catch (Exception e1) {}
				result.setResult(false);
				result.setError("Internal error");
				return result;
			}
		}
		if(transaction.getCompte().isOverdraft(transaction.getAmount())) {
			result.setResult(false);
			result.setError("Account overdraft");
			return result;
		}
		
		try {
			tx.begin();
			transaction.getCompte().debit(transaction.getAmount());
			Account dest=findAccount(transaction.getAccount_id_to());
			dest.credit(transaction.getAmount());
			transaction.setApplied(1);
			em.merge(transaction.getCompte());
			em.merge(dest);
			em.merge(transaction);
			em.flush();
			result.setResult(true);
			result.setMessage("Transaction successfully executed");
			tx.commit();
			return result;
		}catch (Exception e) {
			try {
				tx.rollback();
			} catch(Exception e1) {}
			result.setResult(false);
			result.setError("Internal error");
			return result;
			
		}
	}
		
	private Account findAccount(Integer account){
		return em.find(Account.class, account);
	}
	
	private User findUser(Integer author){
		return em.find(User.class, author);
	}
	
	private boolean isCustomer(User u) {
		return u instanceof Customer;
	}
	
	private boolean isOverMax(Double amount) {
		return amount>MAX_VIREMENT;
	}

}
