package com.ejbank.bean;

import java.sql.Date;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.ejbank.entity.Account;
import com.ejbank.entity.Advisor;
import com.ejbank.entity.Customer;
import com.ejbank.entity.Transaction;
import com.ejbank.entity.User;
import com.ejbank.payload.output.AccountAttached;
import com.ejbank.payload.output.AccountDetail;
import com.ejbank.payload.output.AccountList;
import com.ejbank.payload.output.AccountUser;
import com.ejbank.session.AccountSession;

@Stateless
@LocalBean
public class AccountBean implements AccountSession {
	@PersistenceContext(unitName="EJBankPU")
	private EntityManager em;
	
	
	/**
	 * @param User ID
	 * Verify if ID param is associated to a customer return then list of accounts customer
	 * */
	@Override
	public AccountList<AccountUser> getAccountsFromUser(Integer id) {
		Customer tmp=em.find(Customer.class, id);
		AccountList<AccountUser> result=new AccountList<AccountUser>();
		if(tmp==null) {
			result.setError("User is not a customer");
			return result;
		}
		
		for(Account a : tmp.getAccounts()) {
			result.add(new AccountUser(a.getId(), a.getAccount_type().getName(), a.getBalance()));
		}
		return result;
	}
	
	
	/**
	 * @param User ID
	 * Verify if ID is attached to an advisor then return list of accounts attached to the advisor
	 * */
	@Override
	public AccountList<AccountAttached> getAccountsToAdvisor(Integer id) {
		Advisor advisor=em.find(Advisor.class, id);
		AccountList<AccountAttached> result=new AccountList<AccountAttached>();
		if(advisor==null) {
			result.setError("User is not an advisor");
			return result;
		}
		
		for(Customer c : advisor.getCustomers()) {
			TypedQuery<Long> query=em.createNamedQuery(Advisor.findUserTransactionInProgress,Long.class);
			query.setParameter("advisorId", id);
			query.setParameter("customerId",c.getId());
			
			for(Account a : c.getAccounts()) {
				query.setParameter("accountId", a.getId());
				result.add(new AccountAttached(a.getId(), c.getLastname()+" "+c.getFirstname(), a.getAccount_type().getName(), a.getBalance(), query.getSingleResult()));
			}
		}
		return result;
	}

	
	/**
	 * @param User ID, Account ID
	 * Verify existing user and account attached to params, return then detail of user account  
	 * 
	 * */
	@Override
	public AccountDetail getDetailAccount(Integer account_id, Integer user_id) {
		Account account=em.find(Account.class, account_id);
		User user=em.find(User.class, user_id);
		if(account==null) {
			return new AccountDetail("Error: Compte non reconnu");
		}
		if(user==null) {
			return new AccountDetail("Error: Utilisateur non reconnu");
		}
		if(account.getCustomer().getId()!=user.getId() && account.getCustomer().getAdvisor().getId()!=user.getId()) {
			return new AccountDetail("Error: User does not have access to this account");
		}
		LinkedHashMap<Date,Amount> map=groupByTransaction(account_id);
		double interets = calculInterets(map,account);
		return new AccountDetail(account.getCustomer().getFirstname()+" "+account.getCustomer().getLastname(), account.getCustomer().getAdvisor().getFirstname()+" "+account.getCustomer().getAdvisor().getLastname(), account.getAccount_type().getRate(),account.getBalance(),interets);
	}
	
	
	
	/**
	 * @param Account iD
	 * Get  account transaction and group by Date
	 * */
	private LinkedHashMap<Date,Amount> groupByTransaction(Integer account_id) {
		TypedQuery<Transaction> trans=em.createNamedQuery(Transaction.findTolalTransactionAccountOrdered,Transaction.class);
		LocalDate now=LocalDate.now();
		Date startYear=Date.valueOf(now.getYear()+"-01-01");
		Date endYear=Date.valueOf(now.getYear()+"-12-31");
		trans.setParameter("accountId", account_id);
		trans.setParameter("start", startYear);
		trans.setParameter("end", endYear);
		LinkedHashMap<Date, Amount> map=new LinkedHashMap<>();
		for(Transaction t :trans.getResultList()) {
			Amount amount=map.get(t.getDate());
			if(amount==null) {
				if(t.getAccount_id_to()==account_id) {
					map.put(t.getDate(), new Amount(t.getAmount()));
				}else {
					map.put(t.getDate(), new Amount(-t.getAmount()));
				}
			}else {
				if(t.getAccount_id_to()==account_id) {
					amount.add(t.getAmount());
				}else {
					amount.sub(t.getAmount());
				}
			}
		}
		return map;
	}

	
	/**
	 * @param Map<Date,Amount>,Account
	 * Return amount of interest cumulated
	 * */
	
	private double calculInterets(LinkedHashMap<Date,Amount> map,Account compte ) {
		LocalDate now=LocalDate.now();
		Date current=Date.valueOf(now);
		Date startYear=Date.valueOf(now.getYear()+"-01-01");
		double interets=0;
		double last_amount=compte.getBalance();
		for(Entry<Date, Amount> e :map.entrySet()) {
			long days=duration(current,e.getKey());
			double solde=last_amount-e.getValue().getAmount();
			double interetTmp=((solde+e.getValue().getAmount()) *compte.getAccount_type().getRate())/100;
			double cumul=(interetTmp*days)/365;
			interets+=cumul;
			current=e.getKey();
			last_amount=solde;
		}
		if(last_amount>0) {
			long days=duration(current,startYear);
			double interetTmp=(last_amount *compte.getAccount_type().getRate())/100;
			double cumul=(interetTmp*days)/365;
			interets+=cumul;
		}
		return interets;
	}

	
	/**
	 * Return number of days between two dates
	 * */
	private long duration(Date d1,Date d2) {
		long diffInMillies = Math.abs(d1.getTime() - d2.getTime());
		return TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}

}
