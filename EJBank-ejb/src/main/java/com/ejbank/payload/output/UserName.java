package com.ejbank.payload.output;

import javax.ejb.Local;

@Local
public class UserName {
	private String lastname,firstname;
	
	public UserName(String lastname,String firstname) {
		this.lastname=lastname;
		this.firstname=firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getFirstname() {
		return firstname;
	}
	
	
	
}
