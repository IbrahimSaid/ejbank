package com.ejbank.payload.output;

public class AccountAttached {
	private Integer id;
	private String user;
	private String type;
	private Double amount;
	private Long validation;
	
	public AccountAttached(Integer id,String user,String type,Double amount,Long validation) {
		this.id=id;
		this.type=type;
		this.amount=amount;
		this.user=user;
		this.validation=validation;
	}

	public Integer getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public Double getAmount() {
		return amount;
	}

	public String getUser() {
		return user;
	}

	public Long getValidation() {
		return validation;
	}
	
	
	
}
