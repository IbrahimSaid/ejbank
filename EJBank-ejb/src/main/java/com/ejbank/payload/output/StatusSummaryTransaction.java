package com.ejbank.payload.output;

public class StatusSummaryTransaction {
	private boolean result;
	private Double before;
	private Double after;
	private String message;
	private String error;
	
	public StatusSummaryTransaction(boolean result, Double before, Double after,String message) {
		this.result=result;
		this.before=before;
		this.after=after;
		this.message=message;
	}
	public StatusSummaryTransaction(String error) {
		this.error=error;
	}

	public boolean isResult() {
		return result;
	}

	public Double getBefore() {
		return before;
	}

	public Double getAfter() {
		return after;
	}

	public String getMessage() {
		return message;
	}

	public String getError() {
		return error;
	}
	
	
	
}
