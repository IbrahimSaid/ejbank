package com.ejbank.payload.output;


public class AccountUser {
	private Integer id;
	private String type;
	private Double amount;
	
	
	public AccountUser(Integer id,String type,Double amount) {
		this.id=id;
		this.type=type;
		this.amount=amount;
	}

	public Integer getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public Double getAmount() {
		return amount;
	}
	
	

}
