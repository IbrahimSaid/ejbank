package com.ejbank.payload.output;


public class AccountTransaction {
	private Integer id;
	private String user;
	private String type;
	private Double amount;
	
	
	public AccountTransaction(Integer id,String user,String type,Double amount) {
		this.id=id;
		this.type=type;
		this.amount=amount;
		this.user=user;
	}

	public Integer getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public Double getAmount() {
		return amount;
	}
	
	public String getUser() {
		return this.user;
	}

}
