package com.ejbank.payload.output;

import java.util.ArrayList;

public class TransactionList {
	private Long total;
	private final ArrayList<TransactionPayload> transactions=new ArrayList<>();
	private String error;
	
	public Long getTotal() {
		return total;
	}
	public ArrayList<TransactionPayload> getTransactions() {
		return transactions;
	}
	public String getError() {
		return error;
	}
	public void setTotal(Long total) {
		this.total = total;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	public void add(TransactionPayload t) {
		this.transactions.add(t);
	}
}
