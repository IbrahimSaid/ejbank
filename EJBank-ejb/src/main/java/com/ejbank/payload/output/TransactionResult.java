package com.ejbank.payload.output;

public class TransactionResult {
	private boolean result;
	private String message;
	
	public TransactionResult(boolean result, String message) {
		this.result=result;
		this.message=message;
	}

	public boolean isResult() {
		return result;
	}

	public String getMessage() {
		return message;
	}
		
}
