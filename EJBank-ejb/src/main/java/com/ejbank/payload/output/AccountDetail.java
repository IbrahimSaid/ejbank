package com.ejbank.payload.output;

public class AccountDetail {
	private String owner;
	private String advisor;
	private Integer rate;
	private Double interest;
	private Double amount;
	private String error;
	
	public AccountDetail(String owner, String advisor, Integer rate,Double amount,Double interest) {
		this.interest=interest;
		this.owner=owner;
		this.advisor=advisor;
		this.rate=rate;
		this.amount=amount;
	}
	
	public AccountDetail(String error) {
		this.error=error;
	}

	public Double getAmount() {
		return amount;
	}

	public String getOwner() {
		return owner;
	}

	public String getAdvisor() {
		return advisor;
	}

	public Integer getRate() {
		return rate;
	}

	public Double getInterest() {
		return interest;
	}
	
	public String getError() {
		return error;
	}
	
	
	
	
}
