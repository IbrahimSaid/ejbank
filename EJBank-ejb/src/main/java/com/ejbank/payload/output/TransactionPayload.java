package com.ejbank.payload.output;

import java.util.Date;

public class TransactionPayload {
	private Integer id;
	private Date date;
	private String source;
	private String source_user;
	private String destination;
	private String destination_user;
	private Double amount;
	private String author;
	private String comment;
	private String state;
	
	public TransactionPayload(Integer id, Date date,String source, String destination, Double amount,String author,String comment){
		this.id=id;
		this.date=date;
		this.source=source;
		this.destination=destination;
		this.amount=amount;
		this.author=author;
		this.comment=comment;
	}
	
	public Integer getId() {
		return id;
	}
	public Date getDate() {
		return date;
	}
	public String getSource() {
		return source;
	}
	public String getSource_user() {
		return source_user;
	}

	public String getDestination() {
		return destination;
	}
	public String getDestination_user() {
		return destination_user;
	}
	public Double getAmount() {
		return amount;
	}
	public String getAuthor() {
		return author;
	}
	public String getComment() {
		return comment;
	}
	public void setSource_user(String source_user) {
		this.source_user = source_user;
	}

	public void setDestination_user(String destination_user) {
		this.destination_user = destination_user;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	
}
