package com.ejbank.payload.output;

import java.util.ArrayList;

public class AccountList<E> {
	
	private final ArrayList<E> accounts= new ArrayList<E>();
	private String error;
	
	public void add(E a) {
		accounts.add(a);
	}

	public ArrayList<E> getAccounts() {
		return accounts;
	}
	
	public void setError(String error) {
		this.error=error;
	}
	
	public String getError() {
		return this.error;
	}
}
