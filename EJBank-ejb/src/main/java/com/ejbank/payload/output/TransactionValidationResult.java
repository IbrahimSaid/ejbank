package com.ejbank.payload.output;

public class TransactionValidationResult {
	private boolean result;
	private String message;
	private String error;
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public boolean isResult() {
		return result;
	}

	public String getMessage() {
		return message;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
