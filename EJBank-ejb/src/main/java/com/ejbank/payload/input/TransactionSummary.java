package com.ejbank.payload.input;

public class TransactionSummary {
	private Integer source;
	private Integer destination;
	private Double amount;
	private Integer author;
	
	public TransactionSummary(int source,int destination,Double amount,Integer author) {
		this.source=source;
		this.destination=destination;
		this.amount=amount;
		this.author=author;
	}

	public TransactionSummary() {}
	
	public Integer getSource() {
		return source;
	}

	public Integer getDestination() {
		return destination;
	}

	public Double getAmount() {
		return amount;
	}

	public Integer getAuthor() {
		return author;
	}

	public void setSource(Integer source) {
		this.source = source;
	}

	public void setDestination(Integer destination) {
		this.destination = destination;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setAuthor(Integer author) {
		this.author = author;
	}
	
	
	
}
