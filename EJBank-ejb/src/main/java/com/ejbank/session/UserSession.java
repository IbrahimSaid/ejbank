package com.ejbank.session;

import javax.ejb.Local;

import com.ejbank.payload.output.UserName;

@Local
public interface UserSession {
	public UserName findUserById(Integer id);
}
