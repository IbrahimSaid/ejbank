package com.ejbank.session;

import javax.ejb.Local;

import com.ejbank.payload.input.TransactionRequest;
import com.ejbank.payload.input.TransactionSummary;
import com.ejbank.payload.input.TransactionValidate;
import com.ejbank.payload.output.AccountList;
import com.ejbank.payload.output.AccountTransaction;
import com.ejbank.payload.output.StatusSummaryTransaction;
import com.ejbank.payload.output.TransactionList;
import com.ejbank.payload.output.TransactionResult;
import com.ejbank.payload.output.TransactionValidationResult;

@Local
public interface TransactionSession {
	public Long checkBeforeGetNumberOfTransactionToValidate(Integer user_id);
	public AccountList<AccountTransaction> checkBeforeGetAllAccounts(Integer user_id);
	public StatusSummaryTransaction checkSummary(TransactionSummary summary);
	public TransactionResult checkBeforeExecuteOp(TransactionRequest transaction);
	public TransactionList checkBeforeGetListTransaction(Integer account_id,Integer offset,Integer user_id);
	public TransactionValidationResult checkBeforeValidateTransaction(TransactionValidate request);
}
