package com.ejbank.session;


import javax.ejb.Local;

import com.ejbank.payload.output.AccountAttached;
import com.ejbank.payload.output.AccountDetail;
import com.ejbank.payload.output.AccountList;
import com.ejbank.payload.output.AccountUser;

@Local
public interface AccountSession {
	public AccountList<AccountUser> getAccountsFromUser(Integer id);
	public AccountDetail getDetailAccount(Integer account_id,Integer user_id);
	public AccountList<AccountAttached> getAccountsToAdvisor(Integer id);
}
