package com.ejbank.entity;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="ejbank_advisor",schema="ejbank")
@DiscriminatorValue("advisor")
@NamedQueries({ 
	@NamedQuery(name=Advisor.findTotalTransactionInProgress, query="select count(ts) from Advisor ad join ad.customers c join c.accounts a join a.transactions ts where ad.id=:advisorId and ts.applied=0"),
	@NamedQuery(name=Advisor.findUserTransactionInProgress, query = "select count(ts) from Advisor ad join ad.customers c join c.accounts a join a.transactions ts where ad.id=:advisorId and c.id=:customerId and a.id=:accountId and ts.applied=0"),
})
public class Advisor extends User {
	public static final String findTotalTransactionInProgress="findTotalTransactionInProgress";
	public static final String findUserTransactionInProgress="findUserTransactionInProgress";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id",length=11,nullable=false)
	private Integer id;

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name ="advisor_id")
	private Collection<Customer> customers;

	public Collection<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Collection<Customer> customers) {
		this.customers = customers;
	}

}
