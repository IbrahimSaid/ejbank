package com.ejbank.entity;


import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="ejbank_account",schema="ejbank")
@NamedQuery(name=Account.findAllAccounts, query="select a from Account a")
public class Account  {
	public static final String findAllAccounts="findAllAccounts";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id",length=11,nullable=false)
	private Integer id;
	
	@OneToOne(targetEntity = AccountType.class,cascade= CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(name="account_type_id", referencedColumnName = "id")
	private AccountType account_type;
	
	@OneToMany(mappedBy="compte",cascade=CascadeType.ALL)
	private Collection<Transaction> transactions;
	
	
	private Double balance;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="customer_id")
	private Customer customer;

	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setAccount_type(AccountType account_type) {
		this.account_type = account_type;
	}
	
	@Version
	@Column(name="balance",length=11)
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Integer getId() {
		return id;
	}

	public AccountType getAccount_type() {
		return account_type;
	}

	

	public Collection<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(Collection<Transaction> accounts) {
		this.transactions = accounts;
	}

	public Customer getCustomer() {
		return customer;
	}	
	
	public void credit(Double amount) {
		this.setBalance(this.getBalance()+amount);
	}
	
	public void debit(Double amount) {
		this.setBalance(this.getBalance()-amount);
	}
	public boolean isOverdraft(Double amount) {
		return this.getBalance()+this.getAccount_type().getOverdraft()-amount<0;
	}
}
