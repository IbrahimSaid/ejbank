package com.ejbank.entity;


import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({ 
	@NamedQuery(name=Transaction.findUserTransactionList, query = "select t from Transaction t where t.compte.id=:accountId OR t.account_id_to=:accountId"),
	@NamedQuery(name=Transaction.findTolalTransactionAccount, query = "select count(t) from Transaction t where t.compte.id=:accountId OR t.account_id_to=:accountId"),
	@NamedQuery(name=Transaction.findTolalTransactionAccountOrdered, query = "select t from Transaction t where t.applied=1 and t.date>=:start and t.date<=:end and (t.compte.id=:accountId OR t.account_id_to=:accountId) order by t.date desc")
})
@Entity
@Table(name="ejbank_transaction",schema="ejbank")
public class Transaction {
	public static final String findUserTransactionList="findUserTransactionList";
	public static final String findTolalTransactionAccount="findTolalTransactionAccount";
	public static final String findTolalTransactionAccountOrdered="findTolalTransactionAccountOrdered";
	public Transaction(Account compte,Integer to,Integer auth,Double amount,String commment,Integer applied,Date date) {
		this.compte=compte;
		this.account_id_to=to;
		this.author=auth;
		this.amount=amount;
		this.applied=applied;
		this.comment=commment;
		this.date=date;
	}
	public Transaction() {}
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="id",length=11,nullable=false)
	private Integer id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="account_id_from")
	private Account compte;
	
	@Column(name="account_id_to",length =11 , nullable = false)
	private Integer account_id_to;
	
	@Column(name="author",length=11)
	private Integer author;
	
	@Column(name="amount")
	private Double amount;
	
	@Column(name="comment",length=255)
	private String comment;
	
	@Column(name="applied",length=1)
	private Integer applied;
	
	@Column(name="date",nullable = false)
	private Date date;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAuthor() {
		return author;
	}

	public void setAuthor(Integer author) {
		this.author = author;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getApplied() {
		return applied;
	}

	public void setApplied(Integer applied) {
		this.applied = applied;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Account getCompte() {
		return compte;
	}

	public void setCompte(Account compte) {
		this.compte = compte;
	}

	public Integer getAccount_id_to() {
		return account_id_to;
	}

	public void setAccount_id_to(Integer account_id_to) {
		this.account_id_to = account_id_to;
	}
	
	
}
