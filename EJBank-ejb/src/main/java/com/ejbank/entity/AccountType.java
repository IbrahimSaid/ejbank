package com.ejbank.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ejbank_account_type",schema="ejbank")
public class AccountType {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id",nullable=false)
	private Integer Id;
	
	@Column(name="name",length=50)
	private String name;
	
	@Column(name="rate")
	private Integer rate;
	
	@Column(name="overdraft")
	private Integer overdraft;

	public void setId(Integer id) {
		Id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	public void setOverdraft(Integer overdraft) {
		this.overdraft = overdraft;
	}

	public Integer getId() {
		return Id;
	}

	public String getName() {
		return name;
	}

	public Integer getRate() {
		return rate;
	}

	public Integer getOverdraft() {
		return overdraft;
	}
	
	
}
