package com.ejbank.entity;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="ejbank_customer",schema="ejbank")
@DiscriminatorValue("customer")
public class Customer extends User{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id",length=11,nullable=false)
	private Integer id;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name ="customer_id")
	private Collection<Account> accounts;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="advisor_id")
	private Advisor advisor;


	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setAccounts(Collection<Account> accounts) {
		this.accounts = accounts;
	}

	public Collection<Account> getAccounts() {
		return accounts;
	}

	public Advisor getAdvisor() {
		return advisor;
	}
	
	
	
}
