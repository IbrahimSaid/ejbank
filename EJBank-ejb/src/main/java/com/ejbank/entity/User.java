package com.ejbank.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="ejbank_user",schema="ejbank")
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="type",discriminatorType = DiscriminatorType.STRING, length=50)
@DiscriminatorValue("nothing")
public abstract class User implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id",nullable=false)
	private Integer id;

	@Column(name="lastname",length=50)
	private String lastname;
	
	@Column(name="firstname",length=50)
	private String firstname;
	
	@Column(name="type",length=50)
	private String type;

	
	public String getLastname() {
		return lastname;
	}

	public String getFirstname() {
		return firstname;
	}
	public void setId(Integer id) {
		this.id = id;
	}


	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getId() {
		return id;
	}
	
}